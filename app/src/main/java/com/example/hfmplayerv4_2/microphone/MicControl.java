package com.example.hfmplayerv4_2.microphone;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.hfmplayerv4_2.R;
import com.example.hfmplayerv4_2.hfmLogic.Complex;
import com.example.hfmplayerv4_2.hfmLogic.FFTControl;
import com.example.hfmplayerv4_2.hfmLogic.HapticsControl;

import java.util.Arrays;
import java.util.List;

public class MicControl {

    private AudioRecord recorder = null; //must be the only reference to the audioRecordObject

    private int audioSource;
    private int sampleRateInHz;
    private int channelConfig;
    private int audioFormat;
    private int bufferSizeInBytes;

    private int dataCapPerRead;

    private boolean recordingFlag;
    private Thread recordingThread;

    public short[] audioDataBuffer;



    private static volatile MicControl instance;

    private MicControl(){
        if(instance!=null){
            throw new RuntimeException("Singleton of MicControl class already created, use method getInstance to access it");
        }
        this.audioSource = MediaRecorder.AudioSource.MIC;
        this.sampleRateInHz = 44100;
        this.channelConfig = AudioFormat.CHANNEL_IN_MONO;
        this.audioFormat = AudioFormat.ENCODING_PCM_16BIT;
        this.bufferSizeInBytes = 4*(AudioRecord.getMinBufferSize(this.sampleRateInHz,this.channelConfig,this.audioFormat));

        this.recordingFlag = false;

        this.dataCapPerRead = 4096; // next power of 2 after 2250, the number of samples needed to capture a 20hz wave

        if(this.dataCapPerRead > this.bufferSizeInBytes/2){
            this.dataCapPerRead = this.bufferSizeInBytes/2;
            Log.d("MicCtrl_constructor", "dataCapPerRead bigger than buffer size, new value: " + this.dataCapPerRead);
        }

        FFTControl.populateConstantList(dataCapPerRead);

    }

    public static MicControl getInstance(){
        if(instance == null){
            synchronized (MicControl.class){
                if(instance == null){
                    instance = new MicControl();
                }
            }
        }
        return instance;
    }

    public void startRecording(){
        if(this.recorder != null){
            if(this.recordingFlag == false){
                this.recorder.startRecording();
                this.recordingFlag = true;
            }
            this.recordingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    recordingThreadLoop();
                }
            });
            this.recordingThread.start();
            Log.d("MicCtrl_startRecording", "recordingThread started");
        }else{
            Log.d("MicCtrl_startRecording", "startRecording called without AudioRecordObject creating one");
            this.createAudioRecordeObject();
            this.startRecording();
        }
    }

    private void recordingThreadLoop(){

        int counter = 0;

        while(this.recordingFlag){
            if(this.audioDataBuffer == null){
                this.audioDataBuffer = new short[(this.dataCapPerRead)];
            }
            this.recorder.read(this.audioDataBuffer,0,this.dataCapPerRead);
            counter ++;
            Log.d("test", "read done, Counter: " + counter + "  , size of array: " + this.audioDataBuffer.length); //+ " , data:" + Arrays.toString(this.audioDataBuffer));

            HapticsControl.getInstance().calculateHapticsActivation(counter,this.audioDataBuffer);

        }
    }

    public void stopRecording(){
        if(this.recorder != null) {
            this.recordingFlag = false;
            this.recorder.stop();
            this.recordingThread = null;
            Log.d("MicCtrl_startRecording", "recordingThread stopped and deleted");
        }
    }

    public void createAudioRecordeObject(){
        if(this.recorder == null){

            this.recorder = new AudioRecord(this.audioSource, this.sampleRateInHz, this.channelConfig, this.audioFormat, this.bufferSizeInBytes);

        }else{
            Log.d("MicCtrl_startRecording", "createAudioRecordObject called with a AudioRecord object running, Object remade with new values");
           this.destroyAudioRecordObject();
           this.createAudioRecordeObject();
        }
    }

    public void destroyAudioRecordObject(){
        this.stopRecording();
        this.recorder = null;
    }

    public boolean checkRecordAudioPermission(Context context){
        if(ContextCompat.checkSelfPermission(context,Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
            return false;
        }else{
            return true;
        }

    }

    public boolean getRecordingFlag(){
        return this.recordingFlag;
    }


    protected  MicControl readResolve(){return instance;}

}
