package com.example.hfmplayerv4_2.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

import com.example.hfmplayerv4_2.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


public class BluetoothControl {

    private static volatile BluetoothControl instance;
    Set<BluetoothDevice> pairedDevices;

    BluetoothAdapter bltAdapter;
    int REQUEST_ENABLE_BT = 1;
    boolean connectedToDevice;
    BluetoothDevice connectedDevice;
    BluetoothSocket connectedSocket;

    private ConnectionManager connectionManager = null;

    private BluetoothControl(){
        if (instance != null){
            throw new RuntimeException("Singleton of BluetoothControl class instance already created, use method getInstance() to access it");
        }
        bltAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bltAdapter == null){
            Log.d("BluetoothCrl", "Device does not support Bluetooth"); // todo: see if there is a way to show a splash screen telling the user this : implement a toast later
        }else{
            this.updatePairedDevices();
        }
        this.connectedToDevice = false;
    }

    public static  BluetoothControl getInstance(){
        if(instance == null){
            synchronized (BluetoothControl.class){
                if(instance == null){
                    instance = new BluetoothControl();
                }
            }
        }
        return instance;
    }


    protected BluetoothControl readResolve(){
        return instance;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------//

    public void enableBlt(){
        if(!bltAdapter.isEnabled()) {
            bltAdapter.enable();
        }else{
            Log.d("BluetoothCrl", "Trying to enable bluetooth while bluetooth already enabled");
        }
    }

    public void disableBlt(){
        if(bltAdapter.isEnabled()){
            bltAdapter.disable();
        }else{
            Log.d("BluetoothCrl", "Trying to disable bluetooth while bluetoth already disabled");
        }

    }

    public boolean checkIfBltEnabled(){
        return bltAdapter.isEnabled();
    }

    public boolean checkIfConnectedToDevice(){ return this.connectedToDevice;}

    public void updatePairedDevices(){
        this.pairedDevices = bltAdapter.getBondedDevices();
        for (BluetoothDevice device : pairedDevices) {
            Log.d("BluetoothCrl_paired","name: " + device.getName() + "    ---    Mac address: " + device.getAddress());
        }
    }

    public Set<BluetoothDevice> getPairedDevices(){
        return this.pairedDevices;
    }

    public boolean connectdevice(BluetoothDevice deviceToConnect, UUID uuidToConnect){
        if(this.connectedToDevice == true){
            this.disconnectDevice();
        }

        this.discoverOff();

        BluetoothSocket tmpSocket = null;

        try{
            tmpSocket = deviceToConnect.createInsecureRfcommSocketToServiceRecord(uuidToConnect);
        }   catch (IOException e){
            Log.e("BluetoothCrl_connect", "Socket not created error: ", e);
            return false;
        }

        this.connectedSocket = tmpSocket;

        try{
            this.connectedSocket.connect(); //todo: this is a blocking call, need to take this out to another treadh later
        }catch (IOException connectionE){
            Log.e("BluetoothCrl_connect", "Socket Could Not Connect, error: ", connectionE);
            try{
                this.connectedSocket.close();
            }catch (IOException closingE){
                Log.e("BluetoothCrl_connect", "Socket could not be closed, error: ", closingE);
            }
            this.connectedSocket = null;
            return false;
        }

        this.connectedDevice = deviceToConnect;
        this.connectedToDevice = true;

        this.connectionManager = new ConnectionManager(this.connectedSocket);

        return true;
    }

    public void disconnectDevice(){
        if( this.connectedToDevice == true){
            try{
                this.connectedSocket.close();
            } catch (IOException closingE) {
                Log.e("BluetoothCrl_disconnect", "Socket could not be closed, error: ", closingE);
            }
            this.connectedToDevice = false;
            this.connectedDevice = null;
            this.connectedSocket = null;
            this.connectionManager = null;
        }
    }

    public void sendData(byte[] data){
        if(this.connectionManager != null){
            this.connectionManager.sendDataInternal(data);
        }else{
            Log.e("BluetoothCrl_sendData", "sendData called without an active connectionManager");
        }

    }

    public String getConnectedDeviceName(){
        if(this.connectedToDevice){
            return this.connectedDevice.getName();
        }else{
            return "";
        }
    }

    public boolean discoverOn() {
        return bltAdapter.startDiscovery();
    }

    public boolean discoverOff() {
        return bltAdapter.cancelDiscovery();
    }


    //todo: implement a search for avaliable devices

    //-------------------------------------------------------------------------------------------------------------------------------------

    public static class ConnectDeviceSelector extends PreferenceFragmentCompat {

        PreferenceCategory pairedDevicesCategory;
        BluetoothControl bltControl;
        Set<BluetoothDevice> pairedDevices;
        Map<String, BluetoothDevice> mapedPairedDevices;
        Set<Preference> pairedDevicesPreferences;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

            this.bltControl = BluetoothControl.getInstance();
            this.pairedDevices = bltControl.getPairedDevices();
            this.mapedPairedDevices = new ConcurrentHashMap<String,BluetoothDevice>();

            for(BluetoothDevice device: this.pairedDevices){
                this.mapedPairedDevices.put(device.getAddress(),device);
            }


            this.pairedDevicesPreferences = new HashSet<Preference>();

            Context context = getPreferenceManager().getContext();
            PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);

            this.pairedDevicesCategory = new PreferenceCategory(context);
            this.pairedDevicesCategory.setTitle("Paired Devices");
            this.pairedDevicesCategory.setKey("Paired_Devices_Category");
            screen.addPreference(this.pairedDevicesCategory);

            for (BluetoothDevice device : this.pairedDevices){
                Preference tmpPref = new Preference(context);
                tmpPref.setTitle(device.getName());
                tmpPref.setKey(device.getAddress());
                tmpPref.setSummary(device.getAddress());
                this.pairedDevicesPreferences.add(tmpPref);
            }

            for (Preference devicePref : this.pairedDevicesPreferences){
                this.pairedDevicesCategory.addPreference(devicePref);
                devicePref.setOnPreferenceClickListener(preference -> {

                    //Todo: modify the set of bluetooth devices to a map or list so it is acessible to a lookup

                    BluetoothControl.getInstance().connectdevice(mapedPairedDevices.get(devicePref.getKey()), UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"));
                    getActivity().getSupportFragmentManager().popBackStack();
                    return true;
                });
            }

            setPreferenceScreen(screen);

        }

    }

    //---------------------------------------------------------------------------------------------------------------------------

    private class ConnectionManager extends Thread {

        private final BluetoothSocket connectedSocket;
        private final InputStream imputStrm;
        private final OutputStream outputStrm;

        public ConnectionManager(BluetoothSocket socket){
            this.connectedSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try{
                tmpIn = this.connectedSocket.getInputStream();
            }catch (IOException inputStreamError){
                Log.e("BluetoothCrl_connMng", "Imput stream could not be created from socket, error : " , inputStreamError);
            }

            try{
                tmpOut = this.connectedSocket.getOutputStream();
            }catch (IOException outputStreamError){
                Log.e("BluetoothCrl_connMng", "Output stream could not be created from socket, error : " , outputStreamError);
            }

            this.imputStrm = tmpIn;
            this.outputStrm = tmpOut;

        }

        public void sendDataInternal(byte[] data){
            try{
                outputStrm.write(data);
            }catch (IOException sendDataError){
                Log.e("BluetoothCrl_connMng", "Data was not sent, error : " , sendDataError);
            }
        }

    }


}
