package com.example.hfmplayerv4_2.fragments;

import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hfmplayerv4_2.R;
import com.example.hfmplayerv4_2.bluetooth.BluetoothControl;
import com.example.hfmplayerv4_2.hfmLogic.HapticsControl;
import com.example.hfmplayerv4_2.microphone.MicControl;
import com.example.hfmplayerv4_2.ui.viewModels.RealTimeProcessingViewModel;

import java.util.ArrayList;
import java.util.List;

public class RealTimeProcessingFragment extends Fragment {

    private RealTimeProcessingViewModel mViewModel;

    private MicControl micControl;

    public static RealTimeProcessingFragment newInstance() {
        return new RealTimeProcessingFragment();
    }

    private Button hapticSettingsTestButton;

    private Button playButton;

    private LinearLayout hapticsTextViewsLayout;

    private List<TextView> hapticsTextViewsList;

    private TextView bltConnectStatusTextView;

    Thread hapticsProcessThread;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.real_time_processing_fragment, container, false);

        this.hapticSettingsTestButton = view.findViewById(R.id.hapticSettingsButton);
        hapticSettingsTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.realTimeProcessingContainer, new HapticsControl.hapticsSettingsScreen()).addToBackStack(null).commit();
            }
        });

        this.playButton = view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (MicControl.getInstance().getRecordingFlag()) {
                    MicControl.getInstance().stopRecording();

                    //Todo implement better clear haptics method and put a call here
                    byte[] data = new byte[] {(byte)0x00, (byte)0x00 };
                    BluetoothControl.getInstance().sendData(data);

                    ((Button) view).setText("Play");
                } else {
                    MicControl.getInstance().startRecording();
                    ((Button) view).setText("Stop");
                }
            }
        });

        this.hapticsTextViewsLayout = view.findViewById(R.id.hapticsTextViews);

        this.bltConnectStatusTextView = view.findViewById(R.id.bluetoothConnectionStatus);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RealTimeProcessingViewModel.class);
        // TODO: Use the ViewModel

        micControl = MicControl.getInstance();

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!micControl.checkRecordAudioPermission(getContext())) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 1);
        }

        this.micControl.createAudioRecordeObject();

        if(BluetoothControl.getInstance().checkIfConnectedToDevice()){
            this.bltConnectStatusTextView.setText("Device Connected: " + BluetoothControl.getInstance().getConnectedDeviceName());
        }else{
            this.bltConnectStatusTextView.setText("No Bluetooth Device Connected");
        }

        HapticsControl.getInstance().populateHapticsList(-1, getContext());

        this.hapticsTextViewsList = new ArrayList<>();

        for (int i = 0; i < HapticsControl.getInstance().getNumberOfHaptics(); i++) {
            TextView auxText = new TextView(getContext());
            auxText.setText((i + 1) + " --- " + "Press Play To Start");
            this.hapticsTextViewsList.add(auxText);

            this.hapticsTextViewsLayout.addView(auxText);
        }
        this.hapticsProcessThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Thread.sleep(100);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateHapticsTextViews();
                                prepareAndSendDataForBluetooth();
                            }
                        });

                    }
                } catch (InterruptedException e) {

                }

            }
        });
        this.hapticsProcessThread.start();
    }

    @Override
    public void onStop() {
        super.onStop();

        //Todo implement better clear haptics method and put a call here
        byte[] data = new byte[] {(byte)0x00, (byte)0x00 };
        BluetoothControl.getInstance().sendData(data);

        this.micControl.stopRecording();
        this.micControl.destroyAudioRecordObject();

        this.hapticsProcessThread.interrupt();

        this.hapticsTextViewsList.clear();
        this.hapticsTextViewsLayout.removeAllViewsInLayout();
        this.playButton.setText("Play");

    }

    //-------------------------------------------------------------------------------------------------------------------------------------//

    public synchronized void updateHapticsTextViews() {
        if (this.playButton.getText() == "Stop") {
            HapticsControl auxHapticsControl = HapticsControl.getInstance();

            for (int i = 0; i < auxHapticsControl.getNumberOfHaptics(); i++) {
                this.hapticsTextViewsList.get(i).setText((i + 1) + " --- " + auxHapticsControl.getLowerFreqBoundByIndex(i) + " to " + auxHapticsControl.getUpperFreqBoundByIndex(i) +"--- activates:" + auxHapticsControl.getActivationStatusByIndex(i) + "---" + auxHapticsControl.getdBValueOfHapticByIndex(i));
            }

            Log.d("updateHapticsTextViews", "UI text views updated");
        }
    }

    public synchronized void prepareAndSendDataForBluetooth(){
        if (this.playButton.getText() == "Stop") {
            BluetoothControl auxBluetoothControl = BluetoothControl.getInstance();
            HapticsControl auxHapticsControl = HapticsControl.getInstance();

            int numHaptics = auxHapticsControl.getNumberOfHaptics();

            int auxNumBytes = (numHaptics/8);
            if(numHaptics%8 != 0){
                auxNumBytes += 1;
            }

            byte[] data = new byte[auxNumBytes];

            for (int i = 0; i < data.length; i ++){
                data[i] = 0;
            }

            int auxShift = 8;
            for(int i = 0; i < numHaptics;i++){
                if(i-(i%8)+8 > numHaptics){
                    auxShift = numHaptics - i - 1;
                }else{
                    auxShift = (7-(i%8));
                }
                if(auxHapticsControl.getActivationStatusByIndex(((i-(i%8))+auxShift))){
                    data[i/8] += 1;
                }
                if(auxShift > 0){
                    data[i/8] <<= 1;
                }
            }

            if(auxBluetoothControl.checkIfConnectedToDevice()) {
                auxBluetoothControl.sendData(data);
            }
        }
    }

}