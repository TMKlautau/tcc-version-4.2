package com.example.hfmplayerv4_2.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.hfmplayerv4_2.R;
import com.example.hfmplayerv4_2.fragments.RealTimeProcessingFragment;

public class RealTimeProcessing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.real_time_processing_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.realTimeProcessingContainer, RealTimeProcessingFragment.newInstance())
                    .commitNow();
        }
    }
}
