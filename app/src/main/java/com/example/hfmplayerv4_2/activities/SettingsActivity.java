package com.example.hfmplayerv4_2.activities;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.hfmplayerv4_2.R;
import com.example.hfmplayerv4_2.bluetooth.BluetoothControl;

public class SettingsActivity extends AppCompatActivity {

    BluetoothControl bltControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        bltControl = BluetoothControl.getInstance();
    }

    //Todo move this fragment to a file of its own
    public static class SettingsFragment extends PreferenceFragmentCompat {

        BluetoothControl bltControl;
        Preference connectPreference;
        Preference disconnectPreference;
        Preference enableBluetoothPreference;
        Preference testHaptics1Preference;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            this.bltControl = BluetoothControl.getInstance();
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            this.connectPreference = findPreference("connect_bluetooth");
            this.disconnectPreference = findPreference("disconnect_bluetooth");
            this.enableBluetoothPreference = findPreference("enable_bluetooth");
            this.testHaptics1Preference = findPreference("test_haptics");

            connectPreference.setOnPreferenceClickListener(preference -> {
                Log.d("SettingsActivity", "clicked on connect preference");
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.settings, new BluetoothControl.ConnectDeviceSelector()).addToBackStack(null).commit();
                return true;
            });
            disconnectPreference.setOnPreferenceClickListener(preference -> {
                Log.d("SettingsActivity", "clicked on disconnect preference");
                this.bltControl.disconnectDevice();
                this.checkIfConnected();
                return true;
            });
            enableBluetoothPreference.setOnPreferenceClickListener(preference -> {
                Log.d("SettingsActivity", "clicked on enable bluetooth preference");
                bltControl.enableBlt();
                disconnectPreference.setVisible(false);
                testHaptics1Preference.setVisible(false);
                connectPreference.setVisible(true);
                enableBluetoothPreference.setVisible(false);
                return true;
            });
            testHaptics1Preference.setOnPreferenceClickListener(preference -> {

                Thread testThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        for(int i = 0; i < 50; i ++){

                            byte[] data = new byte[] {(byte)0x01};

                            Log.d("SettingsActivityTestHap", "sent signal number : " + (i+1));
                            /*for (byte b: data){
                                Log.d("SettingsActivityTestHap", String.format("0x%20x", b));
                            }*/

                            BluetoothControl.getInstance().sendData(data);

                            try {
                                Thread.sleep(93);
                            }catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }

                    }
                });

                testThread.start();
                return true;
            });
        }

        @Override
        public void onStart(){
            super.onStart();
            this.checkIfConnected();
        }

        void checkIfConnected(){
            if(!bltControl.checkIfBltEnabled()){
                disconnectPreference.setVisible(false);
                connectPreference.setVisible(false);
                testHaptics1Preference.setVisible(false);
                enableBluetoothPreference.setVisible(true);
            }else {
                //todo: set check of connectivity here when implemented
                boolean auxConnect = bltControl.checkIfConnectedToDevice();
                disconnectPreference.setVisible(auxConnect);
                testHaptics1Preference.setVisible(auxConnect);
                connectPreference.setVisible(!auxConnect);
                enableBluetoothPreference.setVisible(false);
            }
        }

    }
}