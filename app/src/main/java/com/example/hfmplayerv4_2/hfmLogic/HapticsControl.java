package com.example.hfmplayerv4_2.hfmLogic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceScreen;

import com.example.hfmplayerv4_2.fragments.RealTimeProcessingFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class HapticsControl {

    private static volatile HapticsControl instance;

    public static class hapticsSettings {
        int lowerFreqBound;
        int upperFreqBound;
        int amplitudeActivationCutoff;

        int lowerIndexToGetMean;
        int upperIndexToGetMean;

        double dBValueRealTime;

        boolean activationStatus;
    }

    private static int numHaptics;

    public List<hapticsSettings> hapticsList;

    HapticsControl(){

        if (instance != null){
            throw new RuntimeException("Singleton of HapticsControl class instance already created, use method getInstance() to access it");
        }

        hapticsList = new ArrayList<>();

        numHaptics = 0;

    }

    public static HapticsControl getInstance(){
        if(instance == null){
            synchronized (HapticsControl.class){
                if(instance == null){
                    instance = new HapticsControl();
                }
            }
        }
        return instance;
    }


    protected HapticsControl readResolve(){
        return instance;
    }

    //-----------------------------------------------------------------------------------------------------------------------------------//

    public void calculateHapticsActivation(int counter, short[] audioDataIn){

        final short[] auxParam = audioDataIn;
        final int auxParam2 = counter;
        Thread auxThread = new Thread(new Runnable() {
            short[] p = auxParam;
            int pCounter = auxParam2;
            @Override
            public void run() {
                List<Complex> arrayComplex = Complex.convertShortArrayToComplex(p);
                List<Complex> auxfft = FFTControl.calculateFFT(arrayComplex);
                List<Double> outputsDB = new ArrayList<>();
                for(int i = 0; i < auxfft.size()/2; i ++){
                    double auxRe = auxfft.get(i).getRe();
                    double auxIm = auxfft.get(i).getIm();
                    Double auxOutput = 10 * Math.log10(Math.sqrt(auxRe*auxRe + auxIm*auxIm));
                    outputsDB.add(auxOutput);
                }

                double[] auxdBArray = new double[hapticsList.size()];

                for (int i = 0; i < hapticsList.size(); i ++){
                    int auxLower = hapticsList.get(i).lowerIndexToGetMean;
                    int auxUpper = hapticsList.get(i).upperIndexToGetMean;

                    double mean = 0;
                    int auxDivisor = 0;

                    for (int j = auxLower; j < auxUpper; j++){
                        mean += outputsDB.get(j);
                        auxDivisor++;
                    }

                    mean = (double) mean/auxDivisor;

                    auxdBArray[i] = mean;

                    if(mean > hapticsList.get(i).amplitudeActivationCutoff){
                        hapticsList.get(i).activationStatus = true;
                    }else{
                        hapticsList.get(i).activationStatus = false;
                    }

                    Log.d("Outputs of read: " + pCounter, " Output Number: "+ (i+1) + " -- Activated: " +hapticsList.get(i).activationStatus + "  --  Value: " + mean);
                }

                updateHapticsdBValues(auxdBArray);

                Log.d("test","outputsCalcFin, Counter: " + pCounter);
            }

        });
        auxThread.start();
    }

    private synchronized void updateHapticsdBValues(double[] value){
        for (int i = 0; i < value.length; i ++){
            hapticsList.get(i).dBValueRealTime = value[i];
        }

    }

    public void setNumberOfHaptics(int num, Context context){

        this.numHaptics = num;

        this.populateHapticsList(num,context);

    }

    public void populateHapticsList(int num, Context context){ //if num is negative it get the last number of haptics saved on memory

        int auxOldNum = this.hapticsList.size();

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);

        if(num < 0){
            num = Integer.parseInt(sharedPreferences.getString("haptics_num_pref", "0"));
        }

        if(num == 0){
            this.hapticsList.clear();
        }else if(num < auxOldNum){
            for( int i = num; i > auxOldNum; i--){
                this.hapticsList.remove(this.hapticsList.get(this.hapticsList.size()-1));
            }
        }


        for( int i = auxOldNum; i < num; i++){
            hapticsSettings tmpSettings = new hapticsSettings();

            tmpSettings.lowerFreqBound = Integer.parseInt(sharedPreferences.getString("lowerBndFreqPrefIndex"+i,"-1"));
            tmpSettings.upperFreqBound = Integer.parseInt(sharedPreferences.getString("upperBndFreqPrefIndex"+i,"-1"));

            tmpSettings.amplitudeActivationCutoff = Integer.parseInt(sharedPreferences.getString("ampPrefIndex" + i, "10000"));

            tmpSettings.activationStatus = false;

            this.hapticsList.add(tmpSettings);

        }

        this.setupHapticsIndexesMean(4096);
    }

    public void setupHapticsIndexesMean(int numberOfSamples){
        double sizeOfSample = (double) 44100/numberOfSamples;

        Log.d("setupHapticsIndexesMean",  "sizeOfSample: " + sizeOfSample);

        for (int i = 0; i < hapticsList.size(); i++){
            hapticsList.get(i).lowerIndexToGetMean = (int)Math.floor(hapticsList.get(i).lowerFreqBound/sizeOfSample);
            hapticsList.get(i).upperIndexToGetMean = (int)Math.floor(hapticsList.get(i).upperFreqBound/sizeOfSample);

            Log.d("setupHapticsIndexesMean",  "Haptics Number: " + i + " lowerIndex: " + hapticsList.get(i).lowerIndexToGetMean + " upperIndex: " + hapticsList.get(i).upperIndexToGetMean);
        }
    }

    public int getNumberOfHaptics(){
        return this.hapticsList.size();
    }

    public double getdBValueOfHapticByIndex(int i){

        if(i < this.hapticsList.size()){
            return this.hapticsList.get(i).dBValueRealTime;
        }else{
            Log.e("getdBValfHapticsByIndex", "Passed index out of bounds");
            return 0;
        }
    }

    public int getLowerFreqBoundByIndex(int i){
        if(i < this.hapticsList.size()){
            return this.hapticsList.get(i).lowerFreqBound;
        }else{
            Log.e("getLowerFreqBoundByI", "Passed index out of bounds");
            return 0;
        }
    }

    public int getUpperFreqBoundByIndex(int i){
        if(i < this.hapticsList.size()){
            return this.hapticsList.get(i).upperFreqBound;
        }else{
            Log.e("getUpperFreqBoundByI", "Passed index out of bounds");
            return 0;
        }
    }

    public boolean getActivationStatusByIndex(int i){
        if(i < this.hapticsList.size()){
            return this.hapticsList.get(i).activationStatus;
        }else{
            Log.e("getActivationStatusByI", "Passed index out of bounds");
            return false;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------------------------//

    public static class hapticsSettingsScreen extends PreferenceFragmentCompat {

        HapticsControl hapticsCtrl;

        boolean hapticSettingsSeted;

        public static PreferenceCategory hapticsValuesCategory;

        public static EditTextPreference hapticsNumPref;

        public static class hapticsSettingsPreferences{
            public PreferenceCategory hapticIndexCategory;
            public EditTextPreference lowerFreqBoundPref;
            public EditTextPreference upperFreqBoundPref;
            public EditTextPreference amplitudeActivationCutoffPref;
        }

        public static List<hapticsSettingsPreferences> hapticsSettingsPreferencesList;

        static PreferenceScreen screen;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String hotkey){

            this.hapticsCtrl = HapticsControl.getInstance();

            this.hapticSettingsSeted = false;

            hapticsSettingsPreferencesList = new ArrayList<>();

            Context context = getPreferenceManager().getContext();
            this.screen = getPreferenceManager().createPreferenceScreen(context);

            this.hapticsNumPref = new EditTextPreference(context);

            this.hapticsNumPref.setTitle("Change number of haptics feedbacks");
            this.hapticsNumPref.setKey("haptics_num_pref");
            this.hapticsNumPref.setSummary("Number of haptics feedback terminals on each side, each module has 8 terminals");
            this.hapticsNumPref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                @Override
                public void onBindEditText(@NonNull EditText editText) {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
            });

            this.hapticsValuesCategory = new PreferenceCategory(context);

            this.hapticsNumPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    if(newValue != null){
                        hapticsSettingsScreen.hapticsValuesCategory.setTitle("Number of Terminals: "+newValue.toString());
                        HapticsControl.hapticsSettingsScreen.updateHapticsValuesList(Integer.parseInt(newValue.toString()), context);
                        return true;
                    }
                    return false;
                }
            });


            this.screen.addPreference(this.hapticsNumPref);
            this.screen.addPreference(this.hapticsValuesCategory);



            setPreferenceScreen(this.screen);
        }

        @Override
        public void  onStart(){
            super.onStart();

            this.hapticsValuesCategory.setTitle("Number of Terminals: "+this.hapticsNumPref.getText());

            HapticsControl.hapticsSettingsScreen.updateHapticsValuesList(Integer.parseInt(this.hapticsNumPref.getText()),getPreferenceManager().getContext());

            if(this.hapticsCtrl.numHaptics <= 0){
                this.hapticSettingsSeted = false;
            }else {
                this.hapticSettingsSeted = true;
            }



        }

        public static void  updateHapticsValuesList(int newNumberOfHaptics, Context context){

            int auxOldNumberOfHaptics = hapticsSettingsPreferencesList.size();

            if(newNumberOfHaptics < auxOldNumberOfHaptics){
                for( int i = newNumberOfHaptics; i < auxOldNumberOfHaptics; i++){
                    screen.removePreference(hapticsSettingsPreferencesList.get(hapticsSettingsPreferencesList.size()-1).hapticIndexCategory);
                    hapticsSettingsPreferencesList.remove(hapticsSettingsPreferencesList.get(hapticsSettingsPreferencesList.size()-1));
                    HapticsControl.getInstance().hapticsList.remove(HapticsControl.getInstance().hapticsList.get(HapticsControl.getInstance().hapticsList.size()-1));
                }
            }

            HapticsControl.getInstance().setNumberOfHaptics(newNumberOfHaptics, context);

            for( int i = auxOldNumberOfHaptics; i < newNumberOfHaptics; i ++){
                hapticsSettingsPreferences tmpHSP = new hapticsSettingsPreferences();

                tmpHSP.hapticIndexCategory = new PreferenceCategory(context);
                tmpHSP.hapticIndexCategory.setTitle("Output number " + (i+1));

                screen.addPreference(tmpHSP.hapticIndexCategory);
                //---------------------------------------------- Setup of amplitude Preferences ------------------------------------//
                tmpHSP.amplitudeActivationCutoffPref = new EditTextPreference(context);
                tmpHSP.amplitudeActivationCutoffPref.setKey("ampPrefIndex" + i);
                tmpHSP.amplitudeActivationCutoffPref.setTitle("Change Amplitude to activate");
                if(HapticsControl.getInstance().hapticsList.get(i).amplitudeActivationCutoff < 0){
                    tmpHSP.amplitudeActivationCutoffPref.setSummary("Amplitude value not set yet, click to set it");
                }else{
                    tmpHSP.amplitudeActivationCutoffPref.setSummary("Current value: " + (HapticsControl.getInstance().hapticsList.get(i).amplitudeActivationCutoff));
                }
                tmpHSP.amplitudeActivationCutoffPref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                    @Override
                    public void onBindEditText(@NonNull EditText editText) {
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                });
                tmpHSP.amplitudeActivationCutoffPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        String tmpStr = preference.getKey();
                        int tmpIndex = Integer.parseInt(tmpStr.replace("ampPrefIndex", ""));
                        HapticsControl.getInstance().hapticsList.get(tmpIndex).amplitudeActivationCutoff = Integer.parseInt(newValue.toString());
                        tmpHSP.amplitudeActivationCutoffPref.setSummary("Current value: " + (HapticsControl.getInstance().hapticsList.get(tmpIndex).amplitudeActivationCutoff));
                        return true;
                    }
                });
                tmpHSP.hapticIndexCategory.addPreference(tmpHSP.amplitudeActivationCutoffPref);
                //-------------------------------------------------------------------------------------------------------------//
                //Todo implement sanitization of entry boundarys so lower bound is always smaller than upper
                //---------------------------------------------- Setup of lower bound freq Preferences ------------------------------------//
                tmpHSP.lowerFreqBoundPref = new EditTextPreference(context);
                tmpHSP.lowerFreqBoundPref.setKey("lowerBndFreqPrefIndex"+i);
                tmpHSP.lowerFreqBoundPref.setTitle("Change Frequency Lower Boundary");
                if(HapticsControl.getInstance().hapticsList.get(i).lowerFreqBound < 0){
                    tmpHSP.lowerFreqBoundPref.setSummary("Lower bound value not set yet, click to set it");
                }else{
                    tmpHSP.lowerFreqBoundPref.setSummary("Current value " + (HapticsControl.getInstance().hapticsList.get(i).lowerFreqBound));
                }
                tmpHSP.lowerFreqBoundPref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                    @Override
                    public void onBindEditText(@NonNull EditText editText) {
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                });
                tmpHSP.lowerFreqBoundPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        String tmpStr = preference.getKey();
                        int tmpIndex = Integer.parseInt(tmpStr.replace("lowerBndFreqPrefIndex", ""));
                        HapticsControl.getInstance().hapticsList.get(tmpIndex).lowerFreqBound = Integer.parseInt(newValue.toString());
                        tmpHSP.lowerFreqBoundPref.setSummary("Current value " + (HapticsControl.getInstance().hapticsList.get(tmpIndex).lowerFreqBound));
                        return true;
                    }
                });
                tmpHSP.hapticIndexCategory.addPreference(tmpHSP.lowerFreqBoundPref);
                //-------------------------------------------------------------------------------------------------------------//
                //---------------------------------------------- Setup of upper bound freq Preferences ------------------------------------//
                tmpHSP.upperFreqBoundPref = new EditTextPreference(context);
                tmpHSP.upperFreqBoundPref.setKey("upperBndFreqPrefIndex"+i);
                tmpHSP.upperFreqBoundPref.setTitle("Change Frequency upper Boundary");
                if(HapticsControl.getInstance().hapticsList.get(i).upperFreqBound < 0){
                    tmpHSP.upperFreqBoundPref.setSummary("Upper bound value not set yet, click to set it");
                }else{
                    tmpHSP.upperFreqBoundPref.setSummary("Current value " + (HapticsControl.getInstance().hapticsList.get(i).upperFreqBound));
                }
                tmpHSP.upperFreqBoundPref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                    @Override
                    public void onBindEditText(@NonNull EditText editText) {
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                });
                tmpHSP.upperFreqBoundPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        String tmpStr = preference.getKey();
                        int tmpIndex = Integer.parseInt(tmpStr.replace("upperBndFreqPrefIndex", ""));
                        HapticsControl.getInstance().hapticsList.get(tmpIndex).upperFreqBound = Integer.parseInt(newValue.toString());
                        tmpHSP.upperFreqBoundPref.setSummary("Current value " + (HapticsControl.getInstance().hapticsList.get(tmpIndex).upperFreqBound));
                        return true;
                    }
                });
                tmpHSP.hapticIndexCategory.addPreference(tmpHSP.upperFreqBoundPref);
                //-------------------------------------------------------------------------------------------------------------//
                hapticsSettingsPreferencesList.add(tmpHSP);
            }
        }

        @Override
        public void onStop(){
            super.onStop();

            HapticsControl.getInstance().setupHapticsIndexesMean(4096);

        }

    }


}
