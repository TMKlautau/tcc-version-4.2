package com.example.hfmplayerv4_2.hfmLogic;

import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

public class Complex {

    private final double im;
    private final double re;

    Complex(double reIn, double imIn){
        this.re = reIn;
        this.im = imIn;
    }

    public static List<Complex> convertShortArrayToComplex(short[] arrayIn){
        List<Complex> auxList = new ArrayList<>();
        for (int i = 0; i < arrayIn.length; i ++){
            auxList.add(new Complex(arrayIn[i],0));
        }
        return auxList;
    }

    public static Complex multiplication(Complex a, Complex b){
        Complex c = new Complex((a.re*b.re - a.im*b.im), (a.re*b.im + a.im*b.re));
        return c;
    }

    public static Complex addition(Complex a, Complex b){
        Complex c = new Complex((a.re+b.re), (a.im+b.im));
        return c;
    }

    public static Complex subtraction(Complex a, Complex b){
        Complex c = new Complex((a.re-b.re), (a.im-b.im));
        return c;
    }

    public double getRe(){
        return this.re;
    }

    public double getIm(){
        return this.im;
    }
}
