package com.example.hfmplayerv4_2.hfmLogic;

import java.util.ArrayList;
import java.util.List;

public class FFTControl {

    //Todo setup singletion here

    static Complex[][] constantMatrix;

    FFTControl(){

    }

    public static List<Complex> calculateFFT(List<Complex> arrayIn){ //arrayIn needs to be checked to be sure its a power of 2 and need to make sure constant Matrix is populated before calling this

        List<Complex> calculatedFFT = recFFT(arrayIn, 0);

        return calculatedFFT;
    }

    static List<Complex> recFFT(List<Complex> arrayIn, int index){

        if(arrayIn.size() == 1){
            return arrayIn;
        }

        List<Complex> evenIndex = new ArrayList<>();
        List<Complex> oddIndex = new ArrayList<>();

        for (int i = 0; i < arrayIn.size(); i ++){
            if( i%2 == 0){
                evenIndex.add(arrayIn.get(i));
            }else{
                oddIndex.add(arrayIn.get(i));
            }
        }

        evenIndex = recFFT(evenIndex, index+1);
        oddIndex = recFFT(oddIndex, index+1);

        List<Complex> auxList = new ArrayList<>();

        for (int i = 0; i < arrayIn.size(); i ++){
            auxList.add(null);
        }

        int auxN = arrayIn.size()/2;

        for (int i = 0; i < evenIndex.size(); i++){
            Complex auxComp = Complex.addition(evenIndex.get(i),Complex.multiplication(FFTControl.constantMatrix[index][i],oddIndex.get(i)));
            auxList.set(i, auxComp);
            auxComp = Complex.subtraction(evenIndex.get(i),Complex.multiplication(FFTControl.constantMatrix[index][i],oddIndex.get(i)));
            auxList.set(i+auxN,auxComp);
        }

        return auxList;
    }

    public static void populateConstantList(int N){

        int auxN = N;

        int auxRowsCounter = 0;

        while (auxN > 1){
            auxN = auxN/2;
            auxRowsCounter++;
        }

        auxN = N;

        constantMatrix = new Complex[auxRowsCounter][];

        for (int i = 0; i < auxRowsCounter; i++){
            auxN = auxN/2;
            double auxPi = -2*Math.PI;
            constantMatrix[i] = new Complex[auxN];
            for (int j = 0; j < auxN; j++){

                double auxRe = Math.cos(auxPi*j/(auxN*2));
                double auxIm = Math.sin(auxPi*j/(auxN*2));

                Complex auxComplex = new Complex(auxRe, auxIm);

                constantMatrix[i][j] = auxComplex;
            }
        }
    }



}
